package com.akira.superplan;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.akira.superplan.adapters.CustomViewPager;
import com.akira.superplan.fragments.CreateProfileFragment;
import com.akira.superplan.fragments.OTPFragment;
import com.akira.superplan.fragments.TourFragment;
import com.akira.superplan.fragments.VerifyNumberFragment;
import com.akira.superplan.utils.Constants;
import com.akira.superplan.utils.EventBusHelper;
import com.akira.superplan.utils.Utils;

public class SigninFragmentActivity extends FragmentActivity {

    private FragmentManager mFragmentManager;
    private FragmentTransaction mFragmentTransaction;
    private Dialog emailsDialog;
    private LinearLayout popupLayout;
    private LinearLayout transparentLayout;
    private EditText mEditText;
    CountDownTimer timer;
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_signin);
        replaceFragment(new TourFragment(), true);
        popupLayout = (LinearLayout) findViewById(R.id.popup_layout);
        transparentLayout = (LinearLayout) findViewById(R.id.transparentLayout);
    }

    public void replaceFragment(final Fragment fragment, final boolean addToBackStack) {
        final String backStateName = fragment.getClass().getName();
        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.fragment_container, fragment);
        if (addToBackStack) {
            mFragmentTransaction.addToBackStack(backStateName);
        }
        mFragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        if (popupLayout.getVisibility() == View.VISIBLE) {
            if (popupLayout.getTag().equals(R.string.verify_popup)) {
                timer.cancel();
                removePopup();
                replaceFragment(new VerifyNumberFragment(), true);
            }
            if (popupLayout.getTag().equals(R.string.emails_popup)) {
                removePopup();
            }
        } else {
            final FragmentManager fragmentManager = getSupportFragmentManager();
            final int count = fragmentManager.getBackStackEntryCount();
            if (count == 1) {
                finish();
            } else {
                super.onBackPressed();

            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBusHelper.registerSubscriber(this);

    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBusHelper.unregisterSubscriber(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void onEvent(final String message) {
        if (!TextUtils.isEmpty(message)) {
            if (message.equals(Constants.SHOW_EMAILS_POPUP)) {
                showSelectEmailsView();
            } else if (message.equals(Constants.OPEN_VERIFY_NUMBER_SCREEN)) {
                replaceFragment(new VerifyNumberFragment(), true);
            } else if (message.equals(Constants.SHOW_AUTHENTICATING_POPUP)) {
                showAuthenticationPopup();
            } else if (message.equals(Constants.OPEN_CREATE_PROFILE)) {
                replaceFragment(new CreateProfileFragment(), true);
            } else if (message.equals(Constants.PERFORM_BACK_PRESS)) {
                onBackPressed();
            }

        }
    }
    public void showSelectEmailsView() {
        final View child = getLayoutInflater().inflate(R.layout.popup_select_emails, null);

        popupLayout.removeAllViews();
        popupLayout.addView(child);
        popupLayout.setTag(getString(R.string.emails_popup));
        Utils.overrideFonts(this, child);

        final RadioGroup radioGroup = (RadioGroup) child.findViewById(R.id.radio_group);
        final Button continueButton = (Button) child.findViewById(R.id.continue_button);
        radioGroup.removeAllViews();
        try {
            final Account[] accounts = AccountManager.get(this).getAccountsByType("com.google");
            int i = 0;
            for (final Account account : accounts) {
                i = i + 1;
                final String name = account.name;
                if (!TextUtils.isEmpty(name)) {
                    final RadioButton radioButton = new RadioButton(this);
                    radioButton.setText(name);
                    radioButton.setId(i + 100);
                    radioGroup.addView(radioButton);
                    if (i == 1) {
                        radioGroup.check(radioButton.getId());
                    }
                }
            }

        } catch (final Exception e) {
        }


        continueButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(final View v) {
                if (radioGroup.getCheckedRadioButtonId() != -1) {

                    final int id = radioGroup.getCheckedRadioButtonId();
                    final View radioButton = radioGroup.findViewById(id);
                    final int radioId = radioGroup.indexOfChild(radioButton);
                    final RadioButton btn = (RadioButton) radioGroup.getChildAt(radioId);
                    final String emailID = (String) btn.getText();
                    removePopup();
                    replaceFragment(new VerifyNumberFragment(), true);
                }
            }

        });
        showPopup();

        CustomViewPager pager = (CustomViewPager) findViewById(R.id.tour_screens);
        pager.setPagingEnabled(false);
    }
    private void showPopup() {
        final Animation bottomUp = AnimationUtils.loadAnimation(this, R.anim.slide_up);

        popupLayout.startAnimation(bottomUp);
        popupLayout.setVisibility(View.VISIBLE);
        transparentLayout.setVisibility(View.VISIBLE);
        transparentLayout.bringToFront();
        popupLayout.bringToFront();
    }

    private void removePopup() {
        final Animation bottomUp = AnimationUtils.loadAnimation(this, R.anim.slide_down);
        popupLayout.startAnimation(bottomUp);
        popupLayout.setVisibility(View.GONE);
        transparentLayout.setVisibility(View.GONE);
    }

    public void showAuthenticationPopup() {
        final View child = getLayoutInflater().inflate(R.layout.popup_authentication_number, null);
        Utils.overrideFonts(this, child);

        popupLayout.removeAllViews();
        popupLayout.addView(child);
        popupLayout.setTag(R.string.verify_popup);
        showPopup();

        timer = new CountDownTimer(Constants.AUTHENTICATING_POPUP_TIME, 1000) {

            @Override
            public void onTick(final long millisUntilFinished) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onFinish() {
                showDonePopup();
            }
        }.start();

        SpannableString ss = new SpannableString("Thanks. We have sent a One Time Password to your number. " +
                "Please give us a moment to verify. Stuck on verification?" +
                " Enter OTP manually.");
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                timer.cancel();
                removePopup();
                replaceFragment(new OTPFragment(), true);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };
        ss.setSpan(clickableSpan, 115, 133, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        ss.setSpan(new ForegroundColorSpan(Color.BLUE), 115, 133, 0);
        TextView textView = (TextView) findViewById(R.id.desc);
        textView.setText(ss);
        textView.setMovementMethod(LinkMovementMethod.getInstance());

    }

    private void showDonePopup() {
        final View child = getLayoutInflater().inflate(R.layout.popup_done, null);
        Utils.overrideFonts(this, child);
        popupLayout.removeAllViews();
        popupLayout.addView(child);
        popupLayout.setTag(R.string.verify_popup);
        showPopup();
        new CountDownTimer(Constants.AUTHENTICATING_POPUP_TIME, 1000) {

            @Override
            public void onTick(final long millisUntilFinished) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onFinish() {
                showOTPPopup();
            }
        }.start();
    }
    private void showOTPPopup() {
        final View child = getLayoutInflater().inflate(R.layout.popup_enter_otp, null);
        Utils.overrideFonts(this, child);
        popupLayout.removeAllViews();
        popupLayout.addView(child);
        popupLayout.setTag(R.string.verify_popup);
        mEditText = (EditText) child.findViewById(R.id.phone_number_view);
        final TextView submitTextView = (TextView) child.findViewById(R.id.submit);
        submitTextView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(final View v) {
                if (validate()) {
                    removePopup();
                    replaceFragment(new OTPFragment(), true);
                } else {
                    return;
                }
            }
        });
        showPopup();

    }
    private void getOTP() {
        final Cursor cursor = getContentResolver().query(Uri.parse("content://sms/inbox"), null, null, null, null);
        if (cursor.moveToFirst()) { // must check the result to prevent
            // exception
            do {
                String msgData = "";
                for (int idx = 0; idx < cursor.getColumnCount(); idx++) {
                    msgData += " " + cursor.getColumnName(idx) + ":" + cursor.getString(idx);
                    if ("address:".equalsIgnoreCase("") && "body:".equalsIgnoreCase("")) {

                    }
                }
                // use msgData
            } while (cursor.moveToNext());
        } else {
            // empty box, no SMS
        }
    }

    public boolean validate() {
        boolean valid = true;

        String otp_number = mEditText.getText().toString();

        if (otp_number.isEmpty() || otp_number.length() < 6 || otp_number.length() > 6) {
            mEditText.setError("Incorrect OTP Number");
            valid = false;
        } else {
            mEditText.setError(null);
        }

        return valid;
    }

}
