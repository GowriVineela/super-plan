package com.akira.superplan.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.akira.superplan.R;
import com.akira.superplan.utils.Constants;
import com.akira.superplan.utils.EventBusHelper;
import com.akira.superplan.utils.Utils;

public class OTPFragment extends Fragment {
    private Button mContinueButton;
    private EditText mEditText;
    public OTPFragment() {
        super();
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_otp, container, false);
        initUi(view);
        return view;
    }

    private void initUi(final View view) {
        if (getActivity() != null && view != null) {
            Utils.overrideFonts(getActivity(), view);
            mContinueButton = (Button) view.findViewById(R.id.continue_button);
            final ImageView imageView = (ImageView) view.findViewById(R.id.back_button);
            imageView.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(final View v) {
                    EventBusHelper.postMessage(Constants.PERFORM_BACK_PRESS);
                }
            });
            mEditText = (EditText) view.findViewById(R.id.phone_number_view);
            mContinueButton.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(final View v) {
                    if(validate()) {
                        EventBusHelper.postMessage(Constants.OPEN_CREATE_PROFILE);
                    } else{
                        return;
                    }
                }
            });

        }
    }

    public boolean validate() {
        boolean valid = true;

        String otp_number = mEditText.getText().toString();

        if (otp_number.isEmpty() || otp_number.length() < 6 || otp_number.length() > 6) {
            mEditText.setError("Incorrect OTP Number");
            valid = false;
        } else {
            mEditText.setError(null);
        }

        return valid;
    }
}
