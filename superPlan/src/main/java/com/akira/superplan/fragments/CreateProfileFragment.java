package com.akira.superplan.fragments;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.akira.superplan.R;
import com.akira.superplan.adapters.ImageAdapter;
import com.akira.superplan.adapters.RegionsListAdapter;
import com.akira.superplan.utils.Constants;
import com.akira.superplan.utils.EventBusHelper;
import com.akira.superplan.utils.Utils;

public class CreateProfileFragment extends Fragment {
    private Button mContinueButton;
    private RelativeLayout mRegionLayout;
    private LinearLayout mOperatorsLayout;
    Gallery gallery;
    ImageAdapter galleryImageAdapter;
    public CreateProfileFragment() {
        super();
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_create_profile, container, false);
        initUi(view);
        return view;
    }

    private void initUi(final View view) {
        if (getActivity() != null && view != null) {
            Utils.overrideFonts(getActivity(), view);
            mContinueButton = (Button) view.findViewById(R.id.continue_button);
            final ImageView imageView = (ImageView) view.findViewById(R.id.back_button);
            //mOperatorsLayout = (LinearLayout) view.findViewById(R.id.operators_layout);
            gallery = (Gallery) view.findViewById(R.id.gallery);
            mRegionLayout = (RelativeLayout) view.findViewById(R.id.region_layout);
            //addOperators();
            imageView.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(final View v) {
                    EventBusHelper.postMessage(Constants.PERFORM_BACK_PRESS);
                }
            });

            gallery = (Gallery) view.findViewById(R.id.gallery);
            galleryImageAdapter= new ImageAdapter(getActivity());
            gallery.setAdapter(galleryImageAdapter);
            gallery.setSpacing(37);
            gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                    Toast.makeText(getActivity(), "pic" + (position + 1) + " selected",
                            Toast.LENGTH_SHORT).show();
                }
            });

            mRegionLayout.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(final View v) {
                    showRegionsDialog();
                }
            });
            mContinueButton.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(final View v) {
                    EventBusHelper.postMessage(Constants.OPEN_CREATE_PROFILE);
                }
            });

        }
    }
    private void addOperators() {
        mOperatorsLayout.removeAllViews();
       for (int i = 0; i <= 4; i++) {
           final View child = getActivity().getLayoutInflater().inflate(R.layout.item_operator, null);
           Utils.overrideFonts(getActivity(), child);
           final TextView textView = (TextView) child.findViewById(R.id.title_text_view);
           textView.setText("Airtel");
           child.setOnClickListener(new OnClickListener() {

               @Override
               public void onClick(final View v) {
               }
           });

           mOperatorsLayout.addView(child);
       }
    }

    private void showRegionsDialog() {
        AlertDialog.Builder builder;
        final AlertDialog dialog;
        builder = new AlertDialog.Builder(getActivity());
        final View child = getActivity().getLayoutInflater().inflate(R.layout.dialog_regions, null);

        builder.setView(child);
        dialog = builder.create();
        final ListView listView = (ListView) child.findViewById(R.id.regions_list);
        listView.setAdapter(new RegionsListAdapter(getActivity()));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

}
