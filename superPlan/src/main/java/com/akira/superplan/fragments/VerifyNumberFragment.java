package com.akira.superplan.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.akira.superplan.R;
import com.akira.superplan.SigninFragmentActivity;
import com.akira.superplan.utils.Constants;
import com.akira.superplan.utils.EventBusHelper;
import com.akira.superplan.utils.Utils;

public class VerifyNumberFragment extends Fragment {
    private Button mContinueButton;
    private EditText mEditText;
    public VerifyNumberFragment() {
        super();
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_verify_number, container, false);
        initUi(view);
        return view;
    }

    private void initUi(final View view) {
        if (getActivity() != null && view != null) {
            Utils.overrideFonts(getActivity(), view);
            mContinueButton = (Button) view.findViewById(R.id.continue_button);
            mEditText = (EditText) view.findViewById(R.id.phone_number_view);
            final ImageView imageView = (ImageView) view.findViewById(R.id.back_button);
            imageView.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(final View v) {
                    EventBusHelper.postMessage(Constants.PERFORM_BACK_PRESS);
                }
            });
            mEditText = (EditText) view.findViewById(R.id.phone_number_view);

            mEditText.setText("  +91   ");
            Selection.setSelection(mEditText.getText(), mEditText.getText().length());
            mEditText.addTextChangedListener(new TextWatcher() {

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (!s.toString().startsWith("  +91   ")) {
                        mEditText.setText("  +91   ");
                        Selection.setSelection(mEditText.getText(), mEditText.getText().length());
                    }
                }
            });

            mContinueButton.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(final View v) {
                    if (validate()) {
                        EventBusHelper.postMessage(Constants.SHOW_AUTHENTICATING_POPUP);
                        mContinueButton.setEnabled(false);
                        mEditText.setEnabled(false);
                    } else {
                        return;
                    }
                }
            });
        }
    }

    public boolean validate() {
        boolean valid = true;

        String number = mEditText.getText().toString();

        if (number.isEmpty() || number.length() < 18 || number.length() > 18) {
            mEditText.setError("Incorrect Number");
            valid = false;
        } else {
            mEditText.setError(null);
        }

        if (number.length() == 10){
            mEditText.getBackground().setColorFilter(getResources().getColor(R.color.blue), PorterDuff.Mode.SRC_ATOP);
        }
        return valid;
    }

}
