package com.akira.superplan.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.akira.superplan.R;
import com.akira.superplan.fragments.CreateProfileFragment;
import com.akira.superplan.fragments.OTPFragment;
import com.akira.superplan.utils.Constants;
import com.akira.superplan.utils.EventBusHelper;
import com.akira.superplan.utils.Utils;

public class TourViewPageAdapter extends PagerAdapter{
    private final Context context;
    private final int PAGE_COUNT = 4;
    Button signinButton;
    public TourViewPageAdapter(final Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public boolean isViewFromObject(final View view, final Object object) {
        return view == (FrameLayout) object;
    }

    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {
        if (context == null) {
            return null;
        }
        final FrameLayout view = (FrameLayout) LayoutInflater.from(context).inflate(R.layout.item_tour, null);
        final FrameLayout parentLayout = (FrameLayout) view.findViewById(R.id.parent_layout);
        final ImageView imageView = (ImageView) view.findViewById(R.id.image_view);
        final TextView titleText = (TextView) view.findViewById(R.id.title);
        final TextView descriptionText = (TextView) view.findViewById(R.id.description);
        signinButton = (Button) view.findViewById(R.id.signin_button);
        final TextView policyText = (TextView) view.findViewById(R.id.policy);
        signinButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(final View v) {
                EventBusHelper.postMessage(Constants.SHOW_EMAILS_POPUP);
                signinButton.setEnabled(false);
                policyText.setEnabled(false);
            }
        });
        Utils.overrideFonts(context, view);
        if (position == 3) {
            signinButton.setVisibility(View.VISIBLE);
            policyText.setVisibility(View.VISIBLE);
            descriptionText.setVisibility(View.GONE);
            titleText.setTextColor(context.getResources().getColor(R.color.dark_gray));
            descriptionText.setTextColor(context.getResources().getColor(R.color.dark_gray));
            titleText.setTextColor(context.getResources().getColor(R.color.dark_gray));

        } else {

            signinButton.setVisibility(View.GONE);
            policyText.setVisibility(View.GONE);
            descriptionText.setVisibility(View.VISIBLE);
            titleText.setTextColor(context.getResources().getColor(R.color.white));
            descriptionText.setTextColor(context.getResources().getColor(R.color.white));

        }

        if (position == 0) {
            imageView.setBackgroundResource(R.drawable.tour_1);
            titleText.setText(R.string.tour_title_1);
            titleText.setTextSize(TypedValue.COMPLEX_UNIT_SP,32);
            descriptionText.setText(R.string.tour_description_1);

        } else if (position == 1) {
            imageView.setBackgroundResource(R.drawable.tour_1);
            titleText.setText(R.string.tour_title_2);
            descriptionText.setText(R.string.tour_description_2);

        } else if (position == 2) {
            imageView.setBackgroundResource(R.drawable.tour_2);
            titleText.setText(R.string.tour_title_3);
            descriptionText.setText(R.string.tour_description_3);

        } else if (position == 3) {
            imageView.setBackgroundResource(R.drawable.logo);
            titleText.setText(R.string.tour_title_4);

            SpannableString ss = new SpannableString("By signing in you agree to Terms. More on Privacy.");
            ClickableSpan termClickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View textView) {
                    Toast.makeText(container.getContext(), "Terms", Toast.LENGTH_LONG).show();
                }
                @Override
                public void updateDrawState(TextPaint ds) {
                    super.updateDrawState(ds);
                    ds.setUnderlineText(false);
                }
            };

            ClickableSpan privacyClickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View textView1) {
                    Toast.makeText(container.getContext(), "Privacy", Toast.LENGTH_LONG).show();
                }
                @Override
                public void updateDrawState(TextPaint ds1) {
                    super.updateDrawState(ds1);
                    ds1.setUnderlineText(false);
                }
            };
            ss.setSpan(termClickableSpan, 27, 32, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            ss.setSpan(privacyClickableSpan, 42, 49, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            ss.setSpan(R.color.dark_gray, 27, 32, 0);
            ss.setSpan(R.color.dark_gray, 42, 49, 0);
            policyText.setText(ss);
            policyText.setMovementMethod(LinkMovementMethod.getInstance());

        }

        ((ViewPager) container).addView(view);

        return view;
    }

    @Override
    public void destroyItem(final ViewGroup container, final int position, final Object object) {
        ((ViewPager) container).removeView((FrameLayout) object);
    }

}
