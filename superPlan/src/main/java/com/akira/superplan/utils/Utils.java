package com.akira.superplan.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.akira.superplan.R;

public class Utils {
	public static void setMediumTypeFace(Context context, View view) {
		Typeface typeface = null;
		if (context != null && view != null) {
			typeface = Typeface.createFromAsset(context.getAssets(),
					"Roboto-Medium.ttf");
			setTypeFace(view, typeface);
		}
	}

	public static void setRegularTypeFace(Context context, View view) {
		Typeface typeface = null;
		if (context != null && view != null) {
			typeface = Typeface.createFromAsset(context.getAssets(),
					"Roboto-Regular.ttf");
			setTypeFace(view, typeface);

		}
	}

	private static void setTypeFace(View view, Typeface typeface) {
		if (typeface != null && view != null) {
			if (view instanceof EditText) {
				((EditText) view).setTypeface(typeface);
			} else if (view instanceof Button) {
				((Button) view).setTypeface(typeface);
			} else if (view instanceof TextView) {
				((TextView) view).setTypeface(typeface);
			} else if (view instanceof RadioButton) {
				((RadioButton) view).setTypeface(typeface);
			}
		}
	}

	public static void setLightTypeFace(Context context, View view) {
		Typeface typeface = null;
		if (context != null && view != null) {
			typeface = Typeface.createFromAsset(context.getAssets(),
					"Roboto-Light.ttf");
			setTypeFace(view, typeface);
		}
	}

	public static void setBoldTypeFace(Context context, View view) {
		Typeface typeface = null;
		if (context != null && view != null) {
			typeface = Typeface.createFromAsset(context.getAssets(),
					"Roboto-Bold.ttf");
			setTypeFace(view, typeface);
		}
	}

	public static void overrideFonts(final Context context, final View view) {
		if (context != null && view != null) {
			try {
				if (view instanceof ViewGroup) {
					final ViewGroup vg = (ViewGroup) view;
					for (int i = 0; i < vg.getChildCount(); i++) {
						final View child = vg.getChildAt(i);
						overrideFonts(context, child);
					}
				} else if (view instanceof TextView || view instanceof EditText
						|| view instanceof Button
						|| view instanceof RadioButton) {
					if (view.getTag() != null) {
						if (view.getTag()
								.toString()
								.equals(context.getResources().getString(
										R.string.regular_text_tag))) {
							setRegularTypeFace(context, view);

						} else if (view
								.getTag()
								.toString()
								.equals(context.getResources().getString(
										R.string.light_text_tag))) {
							setLightTypeFace(context, view);

						} else if (view
								.getTag()
								.toString()
								.equals(context.getResources().getString(
										R.string.bold_text_tag))) {
							setBoldTypeFace(context, view);

						} else if (view
								.getTag()
								.toString()
								.equals(context.getResources().getString(
										R.string.medium_text_tag))) {
							setMediumTypeFace(context, view);

						}
					}
				}
			} catch (Exception e) {
				Logger.log(e.getMessage(), e);
			}
		}

	}
}
