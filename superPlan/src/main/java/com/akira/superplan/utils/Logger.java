package com.akira.superplan.utils;

public class Logger {

	private static final String AUPER_PLAN_LOGGER = "AUPER_PLAN_LOGGER";
	private static final boolean IS_DEBUG_ENABLED = true;

	private Logger() {
		super();
	}

	public static void log(final String message, final Exception e) {
		if (IS_DEBUG_ENABLED) {
			if (e != null) {
				System.err.println(AUPER_PLAN_LOGGER + message
						+ e.getLocalizedMessage());
			} else {
				System.err.println(AUPER_PLAN_LOGGER + message);

			}
		}
	}

}
