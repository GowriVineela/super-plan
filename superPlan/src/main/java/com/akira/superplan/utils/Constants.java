package com.akira.superplan.utils;

public class Constants {
    public static final String OPEN_CREATE_PROFILE = "OPEN_CREATE_PROFILE";
    public static String PERFORM_BACK_PRESS = "PERFORM_BACK_PRESS";
    public static String SHOW_EMAILS_POPUP = "SHOW_EMAILS_POPUP";
    public static String SHOW_AUTHENTICATING_POPUP = "SHOW_AUTHENTICATING_POPUP";

    public static String OPEN_VERIFY_NUMBER_SCREEN = "OPEN_VERIFY_NUMBER_SCREEN";
    public static int AUTHENTICATING_POPUP_TIME = 10000;
}
