package com.akira.superplan;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;

import com.akira.superplan.utils.Logger;

public class SplashScreenActivity extends Activity {
	private Handler splashScreenHandler = null;
	private Runnable inboxScreenRunnable = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_splash_screen);
		requestLaunchNextActivity();

	}

	@Override
	protected void onStart() {
		super.onStart();
	}

	@Override
	public void onBackPressed() {
		if ((splashScreenHandler != null) && (inboxScreenRunnable != null)) {
			splashScreenHandler.removeCallbacks(inboxScreenRunnable);
		}
		super.onBackPressed();
	}

	/**
	 * Decides which activity needs to be launched depends on user's phone
	 * number registration and profile creation statuses
	 */
	private void requestLaunchNextActivity() {

		Intent nextActivityIntent = new Intent(this,
				SigninFragmentActivity.class);

		delayedNextActivity(nextActivityIntent, 1500);
	}

	/**
	 * Waits for the given time before starting the next activity.
	 * 
	 * @param nextActivityIntent
	 *            Intent for next activity.
	 * @param delayBy
	 *            Time is ms by which the start of next activity should be
	 *            delayed.
	 */
	private void delayedNextActivity(final Intent nextActivityIntent,
			long delayBy) {
		// Keep the splash screen for few seconds and then move to the next
		// activity by killing the existing activity
		splashScreenHandler = new Handler();
		inboxScreenRunnable = new Runnable() {
			@Override
			public void run() {
				finish();
				try {
					SplashScreenActivity.this.startActivity(nextActivityIntent);
				} catch (ActivityNotFoundException anfe) {
					Logger.log(SplashScreenActivity.class.getSimpleName(), anfe);
				} catch (Exception e) {
					Logger.log(SplashScreenActivity.class.getSimpleName(), e);

				}
			}
		};
		splashScreenHandler.postDelayed(inboxScreenRunnable, 1500);
	}

}